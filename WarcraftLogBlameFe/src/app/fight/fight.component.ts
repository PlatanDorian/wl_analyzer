import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReportService} from '../report.service';
import { Event } from '../event';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-fight',
  templateUrl: './fight.component.html',
  styleUrls: ['./fight.component.css']
})
export class FightComponent implements OnInit, OnDestroy {
  reportTag: string;
  savedFight: any;
  fightId: string;
  events: Event[];
  loading: number;
  private subscription: Subscription = new Subscription();


  constructor(
    private route: ActivatedRoute,
    private reportService: ReportService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.loading = 3;
    this.reportTag = this.route.snapshot.paramMap.get('report_id');
    this.fightId = this.route.snapshot.paramMap.get('fight_id');
    this.events = [];
    this.subscription.add(this.reportService.obsFight.subscribe(data => {
      this.savedFight = data;
      if (this.savedFight == null) {
        this.reportService.sendReport(this.reportTag).subscribe(response => {
          this.reportService.saveReport(response) ;
          if (response.report.fights) {
            for (const fight of response.report.fights) {
              if (fight.id === Number(this.fightId)) {
                this.reportService.saveFight(fight);
              }
            }
          }
        });
      }

    }));

    this.reportService.healSavesReport(this.reportTag, this.fightId).subscribe(response => {
      this.loading -= 1;
      this.process_new_results(response);
    });
    this.reportService.deathReport(this.reportTag, this.fightId).subscribe(response => {
      this.loading -= 1;
      this.process_new_results(response);
    });
    this.reportService.bossReport(this.reportTag, this.fightId).subscribe(response => {
      this.loading -= 1;
      this.process_new_results(response);
    });

  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  process_new_results(response) {
    const eventsSize = this.events.length;
    let tmpEvents: Event[] = [];
    for (const event of response.events) {
      tmpEvents.push(event as Event);
    }
    tmpEvents = tmpEvents.concat(this.events);
    if (tmpEvents.length > 0) {
      tmpEvents.sort((a, b) => (a.timestamp > b.timestamp) ? 1 : -1);
    }
    // tslint:disable-next-line:triple-equals
    if (this.events.length == eventsSize) {
      this.events = tmpEvents;
    } else {
      this.process_new_results(response);
    }
  }

  timePresent(miliseconds): string {
    let time;
    time = this.reportService.convertMiliseconds(miliseconds);
    return time;
  }

  eventSorter(): void {
    if (this.events.length > 0) {
      this.events.sort((a, b) => (a.timestamp > b.timestamp) ? 1 : -1)
    }
  }

  get_event_color(importance): string{
    switch (importance) {
      case 0: return 'black';   // common
      case 1: return 'brown';   // death
      case 2: return 'green';   // heal_saves
      case 3: return 'blue';    // boss_mechanicks
      case 4: return 'purple';  // boss_hp
      case 5: return 'orange';  // mistakes
      case 6: return 'red';     // big mistakes
      default: return  'black';
    }
  }

  getState(state): string {
    if (state) {
      return 'Kill';
    }
    return 'Wipe';
  }

  return_back(): void {
    this.router.navigate([`reports`, this.reportTag]);
  }

  go_to_wl(): void{
    let url = 'https://www.warcraftlogs.com/reports/' + this.reportTag + '#fight=' +  this.fightId;
    window.open(url, "_blank");
  }

}
