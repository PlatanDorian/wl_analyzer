export class Event {
  tag: string;
  importance: number;
  message: string;
  timestamp: number;
}
