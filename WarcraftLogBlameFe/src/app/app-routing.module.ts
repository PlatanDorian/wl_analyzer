import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainPageComponent } from './main-page/main-page.component';
import { ErrorComponent } from './error/error.component';
import { ReportComponent } from './report/report.component';
import { FightComponent} from './fight/fight.component';


const routes: Routes = [
  { path: 'index', component: MainPageComponent },
  { path: '', redirectTo: '/index', pathMatch: 'full' },
  { path: 'error', component: ErrorComponent},
  { path: 'reports/:report_id', component: ReportComponent },
  { path: 'reports/:report_id/:fight_id', component: FightComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
