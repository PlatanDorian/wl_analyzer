import { Component, OnInit } from '@angular/core';
import { ReportService} from '../report.service';
import { FormControl} from '@angular/forms';
import { Fight } from '../report';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {
  fights: Fight[];
  title = 'WarcraftLogBlameFe';
  InputUrl = new FormControl();
  reportTag: any;
  constructor(
    private reportService: ReportService,
    private router: Router,
  )  { }


  redirectToReport(): void {
    const inputReport = this.InputUrl.value;
    const startPoint = inputReport.indexOf('reports/') + 8;
    const endPoint =  startPoint + 16;
    this.reportTag = inputReport.slice(startPoint, endPoint);
    this.router.navigate([`reports`, this.reportTag]);
  }



  reportClick(fight): void {
    this.reportService.healSavesReport(this.reportTag, fight.id).subscribe(response => {
      console.log(response); } );
  }


  ngOnInit() {

  }

}
