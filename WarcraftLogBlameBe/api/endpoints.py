from services.warcraftlogs_service import WarCraftLogsService
from services.boss_utility_service import BossUtilityService
from services.boss_service import BossService

from flask import jsonify


from services.healer_save_service import HealerSaveService
from services.death_service import DeathService
from services.report_util_service import ReportUtilService
from services.serialize_service import SerializationService


warcraft_logs_service = WarCraftLogsService()
healer_save_service = HealerSaveService(warcraft_logs_service)
death_service = DeathService(warcraft_logs_service)
report_util_service = ReportUtilService()
serialize_service = SerializationService()
boss_utility_service = BossUtilityService(warcraft_logs_service)
boss_service = BossService(warcraft_logs_service, boss_utility_service)


def setup_routes(app, cache):
    @app.route('/')
    def root():
        return "Response"

    @app.route('/report/<report_code>')
    def report_by_code(report_code):
        fights_report = warcraft_logs_service.get_report_fights(report_code)
        model = {'report': fights_report,
                 'report_code': report_code}
        return jsonify(model)

    @app.route('/report/<report_code>/fight/<int:fight_id>/healer_saves')
    @cache.cached()
    def heal_saves(report_code, fight_id):
        # TODO MUST be taken from cache
        report_meta_data = warcraft_logs_service.get_report_fights(report_code)
        current_fight = report_util_service.get_fight_by_id(report_meta_data, fight_id)
        start = current_fight['start_time']
        end = current_fight['end_time']
        report_events = healer_save_service.create_healer_save_report(report_code, report_meta_data, start, end)
        model = {'events': report_events,
                 'fight_id': fight_id,
                 'report_code': report_code}
        return jsonify(serialize_service.serialize(model))

    @app.route('/report/<report_code>/fight/<int:fight_id>/playres_death')
    @cache.cached()
    def players_death(report_code, fight_id):
        # TODO MUST be taken from cache
        report_meta_data = warcraft_logs_service.get_report_fights(report_code)
        current_fight = report_util_service.get_fight_by_id(report_meta_data, fight_id)
        start = current_fight['start_time']
        end = current_fight['end_time']
        report_events = death_service.create_avoidable_death_report(report_code, report_meta_data, start, end)
        model = {'events': report_events,
                 'fight_id': fight_id,
                 'report_code': report_code}
        return jsonify(serialize_service.serialize(model))

    @app.route('/report/<report_code>/fight/<int:fight_id>/boss_event')
    @cache.cached()
    def boss_event(report_code, fight_id):
        # TODO MUST be taken from cache
        report_meta_data = warcraft_logs_service.get_report_fights(report_code)
        current_fight = report_util_service.get_fight_by_id(report_meta_data, fight_id)
        start = current_fight['start_time']
        end = current_fight['end_time']
        report_events = boss_service.create_fast_boss_events_report(report_code, current_fight, report_meta_data, start, end)
        model = {'events': report_events,
                 'fight_id': fight_id,
                 'report_code': report_code}
        return jsonify(serialize_service.serialize(model))
