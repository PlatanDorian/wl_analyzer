import datetime


class EventLog:

    def __init__(self, tag, timestamp, message,  importance=0, warcraft_logs_url=None):
        self.message = message
        self.timestamp = timestamp
        self.warcraft_logs_url = warcraft_logs_url
        self.tag = tag
        self.importance = importance

    def get_timing(self):
        td = datetime.timedelta(milliseconds=self.timestamp)
        return str(td)
