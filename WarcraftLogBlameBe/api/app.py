from flask import Flask
from flask_cors import CORS
from flask_caching import Cache
from config import Config
import endpoints


def create_app(config):
    main_app = Flask(__name__)
    main_app.config.from_object(config)
    main_app.app_context().push()
    config = main_app.config.copy()
    cache = Cache(main_app)
    endpoints.setup_routes(main_app, cache)
    return main_app


if __name__ == '__main__':

    # only for development
    app = create_app(Config)
    CORS(app)
    app.run(host='0.0.0.0')
