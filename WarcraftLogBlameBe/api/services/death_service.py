from model.EventLog import EventLog

class DeathService:

    warcraft_logs_service = None

    def __init__(self, warcraftlogs_service):
        self.warcraft_logs_service = warcraftlogs_service

    def create_avoidable_death_report(self, report_code, fights_report, start, end):
        heroes = fights_report['friendlies']
        death_list = self.warcraft_logs_service.get_death_list(report_code, start, end)
        events = []

        index = 1
        for death_event in death_list.get('events'):
            target_id = death_event['targetID']
            hero = self.get_friend_by_id(heroes, target_id)
            if hero is not None:

                killer = death_event['killingAbility']['name'] if 'killingAbility' in death_event else 'Неизвестно'
                hero_name = hero['name']
                timestamp = death_event['timestamp'] - start
                events.append(EventLog('player_death', timestamp, "Смерть {0} от [{1}]. ".format(hero_name, killer),
                                       1))
                index += 1
        return events

    @staticmethod
    def get_friend_by_id(wc_logs_objects, object_id):
        for wc_logs_object in wc_logs_objects:
            if wc_logs_object['id'] == object_id:
                return wc_logs_object
        return None
