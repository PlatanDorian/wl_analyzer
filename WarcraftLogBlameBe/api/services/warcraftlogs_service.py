import requests
import json


class WarCraftLogsService:

    # TODO move this to env variable
    api_token = 'ea71e407bbf895f636ae2797779ff27d'
    api_url_base = 'https://www.warcraftlogs.com:443/v1'
    headers = {'Content-Type': 'application/json',
               'Authorization': 'Bearer {0}'.format(api_token)}

    def __init__(self):
        pass

    def get_report_fights(self, report_code):
        api_url = '{0}/report/fights/{1}?api_key={2}'.format(self.api_url_base, report_code, self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_death_list(self, report_code, start, end):
        api_url = '{0}/report/events/deaths/{1}?start={start}&end={end}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code,
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_damage_by_enemy_list(self, report_code, start, end):
        api_url = '{0}/report/tables/damage-taken/{1}?hostility=1&start={start}&end={end}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code,
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_enemy_casts_list_events(self, report_code, ability_id,  start, end):
        api_url = '{0}/report/events/casts/{report_code}?hostility=1&abilityid={abilityid}&start={start}&end={end}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code=report_code,
                    abilityid=ability_id,
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_enemy_casts_list(self, report_code, ability_id,  start, end):
        api_url = '{0}/report/tables/casts/{report_code}?hostility=1&abilityid={abilityid}&start={start}&end={end}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code=report_code,
                    abilityid=ability_id,
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_casts_list(self, report_code, ability_id,  start, end):
        api_url = '{0}/report/events/casts/{report_code}?abilityid={abilityid}&start={start}&end={end}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code=report_code,
                    abilityid=ability_id,
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_casts_list_by_hero(self, report_code, hero_id, ability_id,  start, end):
        api_url = '{0}/report/events/casts/{report_code}?sourceid={sourceid}&abilityid={abilityid}&start={start}&end={end}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code=report_code,
                    sourceid=hero_id,
                    abilityid=ability_id,
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_boss_health(self, report_code, start, end):
        api_url = '{0}/report/tables/resources/{report_code}?abilityid={abilityid}&hostility=1&start={start}&end={end}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code=report_code,
                    abilityid='1000',
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_debufs_events_on_heroes(self, report_code, ability_id,  start, end):
        api_url = '{0}/report/events/debuffs/{report_code}?abilityid={abilityid}&start={start}&end={end}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code=report_code,
                    abilityid=ability_id,
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_debufs_list_on_heroes(self, report_code, ability_id,  start, end):
        api_url = '{0}/report/tables/debuffs/{report_code}?abilityid={abilityid}&start={start}&end={end}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code=report_code,
                    abilityid=ability_id,
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            return body
        else:
            raise Exception

    def get_damage_receive_events(self, report_code, ability_id,  start, end):
        api_url = '{0}/report/events/damage-taken/{report_code}?abilityid={abilityid}&start={start}&end={end}&api_key={api_key}'\
            .format(self.api_url_base,
                    report_code=report_code,
                    abilityid=ability_id,
                    start=start,
                    end=end,
                    api_key=self.api_token)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == 200:
            body = json.loads(response.content.decode('utf-8'))
            if 'nextPageTimestamp' in body:
                next = self.get_damage_receive_events(report_code, ability_id, body['nextPageTimestamp'], end)
                toappend = next['events']
                body['events'].extend(toappend)
            return body
        else:
            raise Exception
