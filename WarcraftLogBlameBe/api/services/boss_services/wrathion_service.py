from model.EventLog import EventLog


class WrathionService:

    def __init__(self, warcraft_logs_service, boss_utility_service):
        self.warcraft_logs_service = warcraft_logs_service
        self.boss_utility_service = boss_utility_service


    @staticmethod
    def get_encounter_id():
        return 2329

    def create_fast_boss_events_report(self, report_code, fights_report, start, end):

        events = []
        heroes = fights_report['friendlies']
        scale_debuf = self.warcraft_logs_service.get_debufs_events_on_heroes(report_code, 307013, start, end)['events']
        scales_per_player = {}
        for event in scale_debuf:
            if event['targetID'] in scales_per_player:
                scales_per_player[event['targetID']].append(event)
            else:
                scales_per_player[event['targetID']] = [event]

        for player in scales_per_player.keys():
            hero = self.get_friend_by_id(heroes, player)
            stack = 0
            apply_time = 0
            for event in scales_per_player[player] :
                if event['type'] == 'applydebuff':
                    apply_time = event['timestamp']
                    stack = 0
                elif event['type'] == 'removedebuffstack':
                    stack += 1
                elif event['type'] == 'removedebuff':
                    if (event['timestamp'] - apply_time) < 10000 + (stack*1000):
                        stack += 1
                        events.append(EventLog('boss_event', event['timestamp'] - start, ' Игрок {} разрушил {} осколков '.format(hero['name'], stack), 0))
                    else:
                        events.append(EventLog('boss_event', event['timestamp'] - start,
                                               ' Игрок {} МЕДЛИЛ и разрушил {} осколков '.format(hero['name'], stack), 5))

        boss_guid = 156818
        checkpoints = [80, 60, 40, 20, 15, 10, 5]

        health_events = self.boss_utility_service.create_boss_health_report(report_code, fights_report,
                                                                            start, end, checkpoints, boss_guid)
        events.extend(health_events)

        return events

    @staticmethod
    def get_friend_by_id(wc_logs_objects, object_id):
        for wc_logs_object in wc_logs_objects:
            if wc_logs_object['id'] == object_id:
                return wc_logs_object
        return None

    def create_detailed_boss_events_report(self, report_code, fights_report, start, end):
        pass