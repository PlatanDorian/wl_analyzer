from model.EventLog import EventLog


class SkitraService:

    def __init__(self, warcraft_logs_service, boss_utility_service):
        self.warcraft_logs_service = warcraft_logs_service
        self.boss_utility_service = boss_utility_service

    @staticmethod
    def get_encounter_id():
        return 2334

    def create_fast_boss_events_report(self, report_code, fights_report, start, end):
        events = []
        enemy_casts_list = self.warcraft_logs_service.get_enemy_casts_list_events(report_code, 309687, start, end)['events']

        fear_counter = 0
        for cast in enemy_casts_list:
            if cast['type'] == 'cast':
                fear_counter += 1
                timestamp = cast['timestamp'] - start
                events.append(EventLog('boss_event', timestamp, 'Большое аое от ада {}'.format(fear_counter), 3))

        checkpoints = [80, 60, 40, 20]
        boss_guid = 157238
        health_events = self.boss_utility_service.create_boss_health_report(report_code, fights_report,
                                                                            start, end, checkpoints, boss_guid)
        for event in health_events:
            event.message += ' [Cмена фазы] '
        health_events.pop()
        events.extend(health_events)
        checkpoints = [10, 5, 2, 1]
        boss_guid = 157238
        health_events = self.boss_utility_service.create_boss_health_report(report_code, fights_report,
                                                                            start, end, checkpoints, boss_guid)
        events.extend(health_events)
        return events

    def create_detailed_boss_events_report(self, report_code, fights_report, start, end):
        pass
