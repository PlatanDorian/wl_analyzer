from model.EventLog import EventLog


class BossUtilityService:

    warcraft_logs_service = None

    def __init__(self, warcraftlogs_service):
        self.warcraft_logs_service = warcraftlogs_service

    def create_boss_health_report(self, report_code, fights_report, start, end, incheckpoints, boss_guid):
        events = []
        checkpoints = incheckpoints.copy()
        enemy_health = self.warcraft_logs_service.get_boss_health(report_code, start, end)['series']
        boss_health = None
        for boss in enemy_health:
            if boss['guid'] == boss_guid:
                boss_health = boss
                # print(start, end, checkpoints, boss_health)
        if boss_health:
            for health_data in boss_health['data']:
                health_data_timestamp = health_data[0]-start
                if len(checkpoints) > 0:
                    if checkpoints[0] >= health_data[1]:
                        events.append(EventLog('boss_health', health_data_timestamp, "У босса осталось {}% хп.".format(
                                    health_data[1]), 4))
                        checkpoints.remove(checkpoints[0])
            last = boss_health['data'][-1]
            if last[1] == 0:
                word_status = 'Kill'
                index_status = 2
            else:
                word_status = 'Wipe'
                index_status = 6
            events.append(EventLog('boss_health', last[0]-start, "У босса осталось {0}% {1}".format(
                            last[1], word_status), index_status))
        return events
