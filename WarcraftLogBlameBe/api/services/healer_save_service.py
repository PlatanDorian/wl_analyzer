from model.EventLog import EventLog


class HealerSaveService:

    warcraft_logs_service = None

    def __init__(self, warcraftlogs_service):
        self.warcraft_logs_service = warcraftlogs_service

    def create_healer_save_report(self, report_code, fights_report, start, end):
            heroes = fights_report['friendlies']
            events = []

            context = {
                "report_code": report_code,
                "start": start,
                "end": end,
                "heroes": heroes,
            }
            events.extend(self.get_heal_save(context, "Мрак", 196718))
            events.extend(self.get_heal_save(context, "Ободряющий Клич ", 97462))
            events.extend(self.get_heal_save(context, "Мастер аур", 31821))
            events.extend(self.get_heal_save(context, "CЛТ", 98008))
            # events.extend(self.get_heal_save(context, "Азеритовый сейв", 293032))
            events.extend(self.get_heal_save(context, "Хил тотем", 108280))
            events.extend(self.get_heal_save(context, "Восстановление сил", 115310))
            events.extend(self.get_heal_save(context, "Барьер", 62618))
            events.extend(self.get_chanel_heal_save(context, "Спокойствие", 157982))
            events.extend(self.get_chanel_heal_save(context, "Спасение", 265202))

            return events

    def get_heal_save(self, context, spell_name, spell_id):
        events = []
        casts_done = self.warcraft_logs_service.get_casts_list(context["report_code"], spell_id, context["start"],
                                                               context["end"])['events']
        for cast in casts_done:
            hero = self.get_friend_by_id(context["heroes"], cast['sourceID'])
            events.append(
                EventLog('healer_save', cast['timestamp'] - context["start"],
                         "Сдан {} игроком {} ".format(spell_name, hero['name']), 2))
        return events

    def get_chanel_heal_save(self, context, spell_name, spell_id):
        events = []
        casts_done = self.warcraft_logs_service.get_casts_list(context["report_code"], spell_id, context["start"],
                                                               context["end"])['events']
        timestamp = 0
        for cast in casts_done:
            if cast['type'] == 'begincast':
                if ((cast['timestamp']-timestamp) > 10000) or timestamp == 0:
                    timestamp = cast['timestamp']
                    hero = self.get_friend_by_id(context["heroes"], cast['sourceID'])
                    events.append(EventLog('healer_save',
                                           cast['timestamp'] - context["start"],
                                           "Сдан {} игроком {} ".format(spell_name, hero['name']), 2))
            elif cast['type'] == 'cast':
                if ((cast['timestamp']-timestamp) > 10000) or timestamp == 0:
                    timestamp = cast['timestamp']
                    hero = self.get_friend_by_id(context["heroes"], cast['sourceID'])
                    events.append(EventLog('healer_save',
                                           cast['timestamp'] - context["start"],
                                           "Сдано {} игроком {} ".format(spell_name, hero['name']), 2))
        return events

    @staticmethod
    def get_friend_by_id(wc_logs_objects, object_id):
        for wc_logs_object in wc_logs_objects:
            if wc_logs_object['id'] == object_id:
                return wc_logs_object
        return None

