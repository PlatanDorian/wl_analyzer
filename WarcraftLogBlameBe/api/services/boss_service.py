from model.EventLog import EventLog
from services.boss_services.skitra_service import SkitraService
from services.boss_services.carapace_service import CarapaceService
from services.boss_services.destagath_service import DestagathService
from services.boss_services.hivemind_service import HivemindService
from services.boss_services.ilngynoth_service import IlginothService
from services.boss_services.maut_service import MautService
from services.boss_services.nzoth_service import NzothService
from services.boss_services.raden_service import RadenService
from services.boss_services.shadhar_service import ShadharService
from services.boss_services.vexiona_service import VexionaService
from services.boss_services.wrathion_service import WrathionService
from services.boss_services.xanesh_service import XaneshService

class BossService:

    warcraft_logs_service = None
    boss_services = []

    def __init__(self, warcraftlogs_service, boss_utility_service):
        self.boss_utility_service = boss_utility_service
        self.warcraft_logs_service = warcraftlogs_service
        self.boss_services.append(WrathionService(warcraftlogs_service, boss_utility_service))
        self.boss_services.append(MautService(warcraftlogs_service, boss_utility_service))
        self.boss_services.append(SkitraService(warcraftlogs_service, boss_utility_service))
        self.boss_services.append(DestagathService(warcraftlogs_service, boss_utility_service))
        self.boss_services.append(HivemindService(warcraftlogs_service))
        self.boss_services.append(IlginothService(warcraftlogs_service, boss_utility_service))
        self.boss_services.append(RadenService(warcraftlogs_service, boss_utility_service))
        self.boss_services.append(ShadharService(warcraftlogs_service, boss_utility_service))
        self.boss_services.append(VexionaService(warcraftlogs_service, boss_utility_service))
        self.boss_services.append(XaneshService(warcraftlogs_service, boss_utility_service))
        self.boss_services.append(CarapaceService(warcraftlogs_service))
        self.boss_services.append(NzothService(warcraftlogs_service))

    def create_fast_boss_events_report(self, report_code, current_fight, fights_report, start, end):
        encounter_id = current_fight['boss']
        for service in self.boss_services:
            if service.get_encounter_id() == encounter_id:
                return service.create_fast_boss_events_report(report_code, fights_report, start, end)
        return []

    def create_detailed_boss_events_report(self, report_code, fights_report, start, end):
        pass


    @staticmethod
    def get_friend_by_id(wc_logs_objects, object_id):
        for wc_logs_object in wc_logs_objects:
            if wc_logs_object['id'] == object_id:
                return wc_logs_object
        return None
