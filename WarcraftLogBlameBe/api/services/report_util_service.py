class ReportUtilService:
    def __init__(self):
        pass

    @staticmethod
    def get_fight_by_id(report_meta_data, fight_id):
        report_meta_data['fights'] = list(filter(lambda x: x['boss'] > 0, report_meta_data['fights']))
        selected_fight = report_meta_data['fights'][-1]
        for fight_report in report_meta_data['fights']:
            if fight_report['id'] is fight_id:
                selected_fight = fight_report
        return selected_fight

