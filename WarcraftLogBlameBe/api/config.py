class Config:
    ENV = 'development'
    DEBUG = True
    CACHE_TYPE = "simple"  # Flask-Caching related configs
    CACHE_DEFAULT_TIMEOUT = 1800
    CACHE_THRESHOLD = 100
